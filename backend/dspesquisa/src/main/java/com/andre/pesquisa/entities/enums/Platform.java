package com.andre.pesquisa.entities.enums;

public enum Platform {
    PC, PLAYSTATION, XBOX;
}
